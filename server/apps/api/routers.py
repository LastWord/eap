from rest_framework import routers

from apps.eats.viewsets import DishViewSet
from apps.eats.viewsets import EatViewSet
from apps.eats.viewsets import IngredientViewSet
from apps.test.viewsets import TestViewSet
from apps.users.viewsets import UsersViewSet

router = routers.DefaultRouter()
router.register('test', TestViewSet, base_name='test')
router.register('users', UsersViewSet, base_name='users')
router.register('eats', EatViewSet, base_name='eats')
router.register('dish', DishViewSet, base_name='dish')
router.register('ingredient', IngredientViewSet, base_name='ingredient')

