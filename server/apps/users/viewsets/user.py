from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from apps.users.serializers import UserSerializer


class UsersViewSet(
                mixins.CreateModelMixin,
                mixins.ListModelMixin,
                mixins.RetrieveModelMixin,
                GenericViewSet):
    serializer_class = UserSerializer

    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        token, created = Token.objects.get_or_create(user=serializer.instance)
        return Response({'user': serializer.data, 'token': token.key})
