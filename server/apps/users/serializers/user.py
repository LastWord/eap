from rest_framework.serializers import ModelSerializer
from rest_framework.authtoken.views import Token
from django.contrib.auth.models import User


class UserSerializer(ModelSerializer):
    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'password',)
        extra_kwargs = {'password': {'write_only': True}}
