from rest_framework.serializers import ModelSerializer

from apps.eats.models import Dish


class DishSerializer(ModelSerializer):
    def create(self, validated_data):
        validated_data['owner'] = self.context['request'].user
        owner = super().create(validated_data)
        return owner

    class Meta:
        model = Dish
        fields = '__all__'
        extra_kwargs = {'total_calories': {'read_only': True},
                        'owner': {'write_only': True},
                        }


