from rest_framework.serializers import ModelSerializer

from apps.eats.models import Eat


class EatSerializer(ModelSerializer):
    def create(self, validated_data):
        validated_data['owner'] = self.context['request'].user
        owner = super().create(validated_data)
        return owner

    class Meta:
        model = Eat
        extra_kwargs = {'latitude': {'read_only': True},
                        'longitude': {'read_only': True},
                        'average_cost': {'read_only': True}}
        exclude = ('owner', )
