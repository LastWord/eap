from .eat import EatViewSet
from .dish import DishViewSet
from .ingredient import IngredientViewSet
