from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet

from apps.eats.models import Dish
from apps.eats.models import Eat
from apps.eats.serializers import DishSerializer


class DishPermission(BasePermission):

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        elif request.method == 'POST':
            return Eat.objects.filter(owner=request.user).exists()
        else:
            return True

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class DishViewSet(ModelViewSet):
    permission_classes = (DishPermission, IsAuthenticatedOrReadOnly,)
    serializer_class = DishSerializer
    queryset = Dish.objects.all()
