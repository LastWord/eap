from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.permissions import BasePermission, SAFE_METHODS

from apps.eats.models import Eat
from apps.eats.serializers import EatSerializer


class EatsPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.owner == request.user


class EatViewSet(ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly, EatsPermission,)
    serializer_class = EatSerializer
    queryset = Eat.objects.all()

