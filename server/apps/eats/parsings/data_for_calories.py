import requests
from bs4 import BeautifulSoup


def get_calorie_data():

    url = requests.get('http://www.takzdorovo.ru/db/nutritives/')
    soup = BeautifulSoup(url.text, 'html.parser')

    calorie_data = {}
    table_ids = ('t1706', 't1939', 't1738', 't1863', 't1984',
                 't2141', 't2401', 't2561', 't2583', 't2735',
                 't2785', 't2811')

    for table_id in table_ids:
        table = soup.find(id=table_id)
        rows = table.findChildren('tr')
        for row in rows:
            calorie_data[row.th.string] = row.find(id="calories").string

    return calorie_data
