from django.db import models


class Eat(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(max_length=255)
    opening_time = models.TimeField(auto_now=False, auto_now_add=False)
    closing_time = models.TimeField(auto_now=False, auto_now_add=False)
    address = models.CharField(max_length=255)

    owner = models.ForeignKey(
        'auth.User',
        related_name='place',
        on_delete=models.CASCADE
    )

    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    average_cost = models.FloatField(max_length=255, default=0)
