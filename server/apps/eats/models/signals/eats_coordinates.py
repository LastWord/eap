from django.db.models.signals import pre_save
from django.dispatch import receiver
from yandex_geocoder import Client

from apps.eats.models import Eat


@receiver(pre_save, sender=Eat)
def eats_coordinates(sender, instance, **kwargs):
    if instance.address:
        instance.longitude, instance.latitude = Client.coordinates(instance.address)
