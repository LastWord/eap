from django.db.models import Avg
from django.db.models import Sum
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from apps.eats.models import Dish
from apps.eats.models import Eat


@receiver(m2m_changed, sender=Dish.ingredients.through)
def counting_total_calories(sender, instance, **kwargs):
    instance.total_calories = Dish.objects.filter(id=instance.pk).aggregate(
        Sum('ingredients__calorie_content'))['ingredients__calorie_content__sum']
    instance.save()


@receiver(m2m_changed, sender=Dish)
def counting_average_cost(sender, instance, **kwargs):
    instance.average_cost = Eat.objects.filter(id=instance.pk).aggregate(
        Avg('dishes__price'))['dishes__price__avg']
    instance.save()



