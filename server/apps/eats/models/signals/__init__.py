from .eats_coordinates import eats_coordinates
from .counting import counting_total_calories
from .counting import counting_average_cost
