from django.db import models

from apps.eats.models import Ingredient


class Dish(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(max_length=255)
    total_calories = models.FloatField(default=0.0, null=True)
    price = models.FloatField(default=0)
    ingredients = models.ManyToManyField(to=Ingredient)

    owner = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
        null=True
    )
