from rest_framework.viewsets import GenericViewSet
from rest_framework.viewsets import mixins

from apps.test.models import Test
from apps.test.serializers import TestSerializer


class TestViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    serializer_class = TestSerializer
    queryset = Test.objects.all()
