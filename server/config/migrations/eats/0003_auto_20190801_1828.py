# Generated by Django 2.2.1 on 2019-08-01 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eats', '0002_auto_20190731_2308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eat',
            name='average_cost',
            field=models.FloatField(default=0, max_length=255),
        ),
        migrations.AlterField(
            model_name='eat',
            name='closing_time',
            field=models.TimeField(),
        ),
        migrations.AlterField(
            model_name='eat',
            name='opening_time',
            field=models.TimeField(),
        ),
    ]
